package com.company.model;

import java.util.Objects;

public class Relation {

    private String startCity;
    private String endCity;

    public Relation(String startCity, String endCity) {
        this.startCity = startCity;
        this.endCity = endCity;
    }

    public String getStartCity() {
        return startCity;
    }

    public void setStartCity(String startCity) {
        this.startCity = startCity;
    }

    public String getEndCity() {
        return endCity;
    }

    public void setEndCity(String endCity) {
        this.endCity = endCity;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Relation relation = (Relation) o;
        return getStartCity().equals(relation.getStartCity()) && getEndCity().equals(relation.getEndCity());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getStartCity(), getEndCity());
    }

    @Override
    public String toString() {
        return "Relation{" +
                "startCity='" + startCity + '\'' +
                ", endCity='" + endCity + '\'' +
                '}';
    }
}
