package com.company.model;

public enum VehicleType {
    SUV, CAR, VAN, BUS
}
