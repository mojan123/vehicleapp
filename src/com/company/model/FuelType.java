package com.company.model;

public enum FuelType {
    DIESEL, PETROL
}
