package com.company.model;

public class Vehicle {

    private String vehicleName;
    private FuelType fuelType;
    private VehicleType vehicleType;
    private Boolean airConditioner;
    private Integer maxNumberOfPassengers;

    public Vehicle(String vehicleName, FuelType fuelType, VehicleType vehicleType,
                   ACUnit acUnit, Integer maxNumberOfPassengers) {
        this.vehicleName = vehicleName;
        this.fuelType = fuelType;
        this.vehicleType = vehicleType;
        this.airConditioner = false;
        if (acUnit.equals(ACUnit.AC)) {
            this.airConditioner = true;
        }
        this.maxNumberOfPassengers = maxNumberOfPassengers;
    }

    public String getVehicleName() {
        return vehicleName;
    }

    public void setVehicleName(String vehicleName) {
        this.vehicleName = vehicleName;
    }

    public FuelType getFuelType() {
        return fuelType;
    }

    public void setFuelType(FuelType fuelType) {
        this.fuelType = fuelType;
    }

    public VehicleType getVehicleType() {
        return vehicleType;
    }

    public void setVehicleType(VehicleType vehicleType) {
        this.vehicleType = vehicleType;
    }

    public Boolean getAirConditioner() {
        return airConditioner;
    }

    public void setAirConditioner(Boolean airConditioner) {
        this.airConditioner = airConditioner;
    }

    public Integer getMaxNumberOfPassengers() {
        return maxNumberOfPassengers;
    }

    public void setMaxNumberOfPassengers(Integer maxNumberOfPassengers) {
        this.maxNumberOfPassengers = maxNumberOfPassengers;
    }

    @Override
    public String toString() {
        return "Vehicle{" +
                "vehicleName='" + vehicleName + '\'' +
                ", fuelType=" + fuelType +
                ", vehicleType=" + vehicleType +
                ", airConditioner=" + airConditioner +
                ", maxNumberOfPassengers=" + maxNumberOfPassengers +
                '}';
    }
}
