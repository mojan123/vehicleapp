package com.company;

import com.company.impl.TotalCharges;
import com.company.model.ACUnit;
import com.company.model.FuelType;
import com.company.model.Vehicle;
import com.company.model.VehicleType;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class Main {

    private static TotalCharges totalCharges = new TotalCharges(); // inject as static, NO FRAMEWORK IN THE TASK


    @Test
    public void test1() throws Exception {
        Vehicle vehicle = new Vehicle("VW Golf", FuelType.DIESEL, VehicleType.CAR, ACUnit.NON_AC, 4);
        String relations = "Prague-Brno-Budapest-Prague";
        Integer passengers = 3;

        double total = totalCharges.getTotalCharges(vehicle, relations, passengers);
        assertEquals(1540.0, total, 0.001);
    }

    @Test
    public void test2() throws Exception {
        Vehicle vehicle = new Vehicle("VW Golf", FuelType.PETROL, VehicleType.CAR, ACUnit.AC, 4);
        String relations = "Prague-Brno-Budapest-Prague";
        Integer passengers = 5;

        double total = totalCharges.getTotalCharges(vehicle, relations, passengers);
        assertEquals(2420, total, 0.001);
    }

    @Test
    public void test3() throws Exception {
        Vehicle vehicle = new Vehicle("VW transporter", FuelType.PETROL, VehicleType.BUS, ACUnit.AC, 6);
        String relations = "Prague-Brno-Budapest-Prague";
        Integer passengers = 5;

        double total = totalCharges.getTotalCharges(vehicle, relations, passengers);
        assertEquals(1837, total, 0.001);
    }

    public static void main(String[] args) throws Exception {
    }
}
