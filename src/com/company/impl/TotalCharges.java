package com.company.impl;

import com.company.interfaces.ITotalCharges;
import com.company.model.Relation;
import com.company.model.Vehicle;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class TotalCharges implements ITotalCharges {

    private static Distance distanceDb = new Distance(); // inject as static, NO FRAMEWORK IN THE TASK
    private static StandardRate standardRate = new StandardRate(); // inject as static, NO FRAMEWORK IN THE TASK
    private static AdditionalCharges additionalCharges = new AdditionalCharges(); // inject as static, NO FRAMEWORK IN THE TASK

    @Override
    public double getTotalCharges(Vehicle vehicle, String relations, Integer passengers) throws Exception {
        System.out.println(String.format("getTotalCharges ( vehicle : %s, relations : %s, passengers : %d )",
                vehicle, relations, passengers));

        double charge = 0.0f;

        String[] tokens = relations.split("-");
        for (int i=0; i<tokens.length-1; i++) {
            String startCity = tokens[i];
            String endCity = tokens[i+1];
            Relation relation = new Relation(startCity, endCity);
            charge += standardRate.getStandardRateForVehicle(vehicle, relation);
            charge += additionalCharges.additionalChargesForAC(vehicle, relation);
            charge += additionalCharges.additionalChargesForPersons(vehicle, relation, passengers);
            System.out.println("******");
        }
        charge = round(charge, 2);
        System.out.println("total charges : " + charge);
        return charge;
    }

    private static double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        BigDecimal bd = BigDecimal.valueOf(value);
        bd = bd.setScale(places, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }
}
