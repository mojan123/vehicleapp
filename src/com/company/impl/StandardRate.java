package com.company.impl;

import com.company.interfaces.IStandardRate;
import com.company.model.FuelType;
import com.company.model.Relation;
import com.company.model.Vehicle;
import com.company.model.VehicleType;

public class StandardRate implements IStandardRate {

    private static Distance distanceDb = new Distance(); // inject as static, NO FRAMEWORK IN THE TASK

    @Override
    public double getStandardRateForVehicle(Vehicle vehicle, Relation relation) throws Exception {
        double rate = 1.5f; // standard rate for petrol

        if (vehicle.getFuelType().equals(FuelType.DIESEL)) {
            rate -= 0.1f; // 0.1 euro less for diesel
        }

        if (vehicle.getVehicleType().equals(VehicleType.BUS)) {
            rate *= 0.98f; // 2% discount on bus
        }
        Integer distance = distanceDb.getDistance(relation);
        System.out.println(String.format("Standard rate for vehicle : %s = %f", vehicle, rate*distance));
        return rate*distance;
    }
}
