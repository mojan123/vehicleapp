package com.company.impl;

import com.company.interfaces.IDistance;
import com.company.model.Relation;
import java.util.HashMap;
import java.util.Map;

public class Distance implements IDistance {

    @Override
    public Integer getDistance(Relation relation) throws Exception {
        Map<Relation, Integer> relationsMap = new HashMap<>();
        relationsMap.put(new Relation("Prague", "Brno"), 200);
        relationsMap.put(new Relation("Brno", "Prague"), 200);

        relationsMap.put(new Relation("Prague", "Budapest"), 550);
        relationsMap.put(new Relation("Budapest", "Prague"), 550);

        relationsMap.put(new Relation("Brno", "Viena"), 150);
        relationsMap.put(new Relation("Viena", "Brno"), 150);

        relationsMap.put(new Relation("Brno", "Budapest"), 350);
        relationsMap.put(new Relation("Budapest", "Brno"), 350);

        if (!relationsMap.containsKey(relation)) {
            throw new Exception("No such relation + " + relation);
        }

        Integer distance = relationsMap.get(relation);
        System.out.println(String.format("getDistance ( relation : %s) =  %d", relation, distance));
        return distance;
    }
}
