package com.company.impl;

import com.company.interfaces.IAdditionalCharges;
import com.company.model.Relation;
import com.company.model.Vehicle;

public class AdditionalCharges implements IAdditionalCharges {

    private static Distance distanceDb = new Distance(); // inject as static, NO FRAMEWORK IN THE TASK

    @Override
    public double additionalChargesForAC(Vehicle vehicle, Relation relation) throws Exception {
        double charges = 0.0f;

        if (vehicle.getAirConditioner()) {
            Integer distance = distanceDb.getDistance(relation);
            charges += 0.2f*distance; // 0.2 euro per km for AC
        }
        System.out.println(String.format("additionalChargesForAC( vehicle: %s, relation: %s ) = %f", vehicle, relation, charges));
        return charges;
    }

    @Override
    public double additionalChargesForPersons(Vehicle vehicle, Relation relation, Integer passengers) throws Exception {
        double charges = 0.0f;
        if (vehicle.getMaxNumberOfPassengers() < passengers) {
            Integer distance = distanceDb.getDistance(relation);
            charges += 0.1f*distance*passengers;
        }
        System.out.println(String.format("additionalChargesForPersons( vehicle: %s, relation: %s, passengers: %d ) = %f",
                vehicle, relation, passengers, charges));

        return charges;
    }
}
