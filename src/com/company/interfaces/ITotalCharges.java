package com.company.interfaces;

import com.company.model.Vehicle;

public interface ITotalCharges {

    double getTotalCharges(Vehicle vehicle, String relations, Integer passengers) throws Exception;
}
