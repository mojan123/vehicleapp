package com.company.interfaces;

import com.company.model.Relation;
import com.company.model.Vehicle;

public interface IAdditionalCharges {
    double additionalChargesForAC(Vehicle vehicle, Relation relation) throws Exception;
    double additionalChargesForPersons(Vehicle vehicle, Relation relation, Integer passengers) throws Exception;
}
