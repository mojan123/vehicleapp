package com.company.interfaces;

import com.company.model.Relation;
import com.company.model.Vehicle;

public interface IStandardRate {

    double getStandardRateForVehicle(Vehicle vehicle, Relation relation) throws Exception;
}
