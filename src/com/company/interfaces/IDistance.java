package com.company.interfaces;

import com.company.model.Relation;

public interface IDistance {

    Integer getDistance(Relation relation) throws Exception;
}
